#!/usr/bin/env bash

echo;echo;echo;echo;echo;echo;
echo;echo;echo;echo;echo;echo;
echo;echo;echo;echo;echo;echo;
echo;echo;echo;echo;echo;echo;
echo 'Fri Dec 18 19:40:40 EDT 2015'
echo 'root@metasploitable:/var/tmp/volatility-2.3.1# echo '$1
echo $1
echo 'root@metasploitable:/var/tmp/volatility-2.3.1# free -m'
echo '             total       used       free     shared    buffers     cached'
echo 'Mem:           503        445         58          0          4        297'
echo '-/+ buffers/cache:        143        359'
echo 'Swap:            0          0          0'
echo 'root@metasploitable:/var/tmp/volatility-2.3.1# du -sh /var/tmp/src/mem.img'
echo '513M    /var/tmp/src/mem.img'
echo 'root@metasploitable:/var/tmp/volatility-2.3.1# python2.6 vol.pt --plugins=/var/tmp/volatility-2.3.1/.volatility/profiles/ --profile=LinuxUBUNTU-MSF804x86 linux_lsof -f /var/tmp/src/mem.img | tail -10'
echo 'Volatility Foundation Volatility Framework 2.3.1'
echo '    5595        0 /dev/tty1'
echo '    5595        1 /dev/tty1'
echo '    5595        2 /dev/tty1'
echo '    5596        0 /dev/tty1'
echo '    5596        1 /dev/tty1'
echo '    5596        2 /dev/tty1'
echo '    5596      255 /dev/tty1'
echo '   10411        0 /dev/tty1'
echo '   10411        1 /dev/tty1'
echo '   10411        2 /dev/tty1'
echo 'root@metasploitable:/var/tmp/volatility-2.3.1# _'
echo;echo;echo;echo;echo;echo;
echo;echo;echo;echo;echo;echo;
echo;echo;echo;echo;echo;echo;
echo;echo;echo;echo;echo;echo;
echo;echo;echo;echo;echo;echo;
echo;echo;echo;echo;echo;echo;
echo;echo;echo;echo;echo;echo;